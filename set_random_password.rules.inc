<?php

/**
 * @file
 */

/**
 * Implements hook_rules_action_info().
 */
function set_random_password_rules_action_info() {
  return array(
    'set_random_password_set_password' => array(
      'label' => 'Set random password',
      'parameter' => array(
        'account' => array('type' => 'user', 'label' => t('User')),
      ),
      'group' => t('System'),
    ),
  );
}

/**
 * Rules action callback to login the user provided in the function parameter
 */
function set_random_password_set_password($account) {
  // debug($account);
  global $user;
  $user = user_load($account->uid);

  $passtype = t('Send a password in the email');
  $password = user_password();
  user_save($user, array('pass' => $password));

  drupal_set_message(t("Password set to '@password'.", array('@password' => $password)));
  watchdog('Set random password', t("Password set to '@password'.", array('@password' => $password)));

  drupal_mail(
    'passquickset',
    'passgen',
    $user->mail,
    user_preferred_language($user),
    array('user' => $user, 'password' => $password, 'passtype' => $passtype),
    variable_get('site_mail', NULL),
    TRUE
  );
}

/**
 * Implements hook_default_rules_configuration_alter()
 */
function set_random_password_default_rules_configuration_alter(&$configs) {
  if (!empty($configs['set_random_password'])) {
    $configs['set_random_password']->action(rules_action('set_random_password_set_password', array('account:select' => 'account-fetched:0')));
  }
}
